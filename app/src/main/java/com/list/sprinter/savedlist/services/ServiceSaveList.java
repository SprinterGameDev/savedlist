package com.list.sprinter.savedlist.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.list.sprinter.savedlist.content.DatabaseHelper;
import com.list.sprinter.savedlist.content.providers.LinkedListProvider;

public class ServiceSaveList extends IntentService {

    private enum IntentActions {
        ADD_ITEM,
        DELETE_ITEM,
        MOVE_ITEM,
        UNDEFINED;

        static IntentActions getAction(@NonNull final String name) {
            try {
                return IntentActions.valueOf(name);
            } catch (Exception e) {
                Log.w("ServiceSaveList", "Undefined action name: " + name);
                return IntentActions.UNDEFINED;
            }
        }
    }

    private static final String KEY_ITEM_NAME = "KEY_ITEM_NAME";
    private static final String KEY_ITEM_PAST_BEFORE_ID = "KEY_ITEM_PAST_BEFORE_ID";
    private static final String KEY_ITEM_ID = "KEY_ITEM_ID";

    public ServiceSaveList() {
        super(ServiceSaveList.class.getSimpleName());
    }

    public static void addItem(@NonNull final Context context, @NonNull final String itemName) {
        final Intent intent = new Intent(context, ServiceSaveList.class);
        intent.setAction(String.valueOf(IntentActions.ADD_ITEM.name()));
        intent.putExtra(KEY_ITEM_NAME, itemName);
        context.startService(intent);
    }

    public static void deleteItem(@NonNull final Context context, final int id) {
        final Intent intent = new Intent(context, ServiceSaveList.class);
        intent.setAction(String.valueOf(IntentActions.DELETE_ITEM.name()));
        intent.putExtra(KEY_ITEM_ID, id);
        context.startService(intent);
    }

    public static void moveItem(@NonNull final Context context, final int pastBeforeId,
                                final int movedId) {
        final Intent intent = new Intent(context, ServiceSaveList.class);
        intent.setAction(IntentActions.MOVE_ITEM.name());
        intent.putExtra(KEY_ITEM_PAST_BEFORE_ID, pastBeforeId);
        intent.putExtra(KEY_ITEM_ID, movedId);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null && !TextUtils.isEmpty(intent.getAction())) {
            final IntentActions action = IntentActions.getAction(intent.getAction());
            switch (action) {
                case ADD_ITEM:
                    final String value = intent.getStringExtra(KEY_ITEM_NAME);
                    if (!TextUtils.isEmpty(value)) {
                        addItem(value);
                    }
                    break;

                case DELETE_ITEM:
                    final int deleteId = intent.getIntExtra(KEY_ITEM_ID, 0);
                    if (deleteId > 0) {
                        deleteItem(deleteId);
                    }
                    break;

                case MOVE_ITEM:
                    final int pastBeforeId = intent.getIntExtra(KEY_ITEM_PAST_BEFORE_ID, 0);
                    final int movedId = intent.getIntExtra(KEY_ITEM_ID, 0);
                    if (pastBeforeId != 0 && movedId > 0) {
                        moveItems(pastBeforeId, movedId);
                    }
                    break;

                default:
                    Log.w(getClass().getSimpleName(), "undefined name of action: " + intent.getAction());

            }
        }
    }

    private void addItem(@NonNull final String value) {
        final ContentValues values = new ContentValues();
        values.put(DatabaseHelper.VALUE_NAME_COLUMN, value);
        getContentResolver().insert(LinkedListProvider.CONTENT_URI, values);
    }

    private void deleteItem(final int id) {
        getContentResolver().delete(LinkedListProvider.CONTENT_URI, String.valueOf(id), null);
    }

    private void moveItems(final int pastBeforeId, final int movedId) {
        final ContentValues values = new ContentValues();
        values.put(LinkedListProvider.VALUE_UPDATE_PAST_BEFORE_ID, pastBeforeId);
        values.put(LinkedListProvider.VALUE_UPDATE_MOVED_ID, movedId);
        getContentResolver().update(LinkedListProvider.CONTENT_URI, values, null, null);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
