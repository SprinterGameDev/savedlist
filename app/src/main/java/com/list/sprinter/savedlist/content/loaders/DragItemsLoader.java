package com.list.sprinter.savedlist.content.loaders;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;

import com.list.sprinter.savedlist.content.contracts.DragItemsList;
import com.list.sprinter.savedlist.content.providers.LinkedListProvider;

public final class DragItemsLoader extends AsyncTaskLoader<DragItemsList> {

    private final ForceLoadContentObserver mObserver;

    @NonNull
    private final DragItemsList mItemsList;

    public DragItemsLoader(Context context) {
        super(context);
        mItemsList = new DragItemsList();
        mObserver = new ForceLoadContentObserver();
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged() || mItemsList.isEmpty()) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
    }

    @Override
    public DragItemsList loadInBackground() {
        final ContentResolver contentResolver = getContext().getContentResolver();
        final Cursor cursor = contentResolver.query(LinkedListProvider.CONTENT_URI,
                LinkedListProvider.DEFAULT_PROJECTION, /* selection */ null,
                /* selectionArgs */ null, /* sortOrder */ null);
        if (cursor != null) {
            cursor.registerContentObserver(mObserver);
            mItemsList.clear();
            mItemsList.addAll(cursor);
        }
        return mItemsList;
    }

}
