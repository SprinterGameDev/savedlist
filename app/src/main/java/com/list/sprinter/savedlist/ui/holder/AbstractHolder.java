package com.list.sprinter.savedlist.ui.holder;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import butterknife.ButterKnife;

public abstract class AbstractHolder<D> extends RecyclerView.ViewHolder implements
        View.OnClickListener, View.OnTouchListener {

    public static final int NO_RES_ID = -1;

    D mModel;
    private IItemClickListener<D> mClickListener;
    private boolean isSelected;
    private IItemTouchListener<D> mTouchListener;

    /* package */ AbstractHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public abstract void bind();

    /**
     * Prepares view holder to be recycled.
     */
    public void unbind() {
        // Could be abstract if will be useful
    }

    public void setSelected(final boolean selected) {
        isSelected = selected;
    }

    public void setData(D model) {
        this.mModel = model;
        ButterKnife.bind(this, itemView);
        bind();
    }

    private void setClickableItems(@NonNull @IdRes final int[] items) {
        final View.OnClickListener listener = mClickListener == null ? null : this;

        for (int id : items) {
            final View view = id == NO_RES_ID ? itemView : ButterKnife.findById(itemView, id);
            if (view != null) {
                view.setOnClickListener(listener);
                view.setClickable(true);
                view.setEnabled(true);
            }
        }
    }

    private void setTouchItems(@NonNull @IdRes final int[] items) {
        final View.OnTouchListener listener = mClickListener == null ? null : this;

        for (int id : items) {
            final View view = id == NO_RES_ID ? itemView : ButterKnife.findById(itemView, id);
            if (view != null) {
                view.setOnTouchListener(listener);
                view.setClickable(true);
                view.setEnabled(true);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (mClickListener != null) {
            mClickListener.onItemClick(mModel, view, null);
        }
    }

    @Override
    public boolean onTouch(@NonNull final View view, @NonNull final MotionEvent event) {
        if (mTouchListener != null) {
            mTouchListener.onItemTouch(this, mModel, event, view, null);
        }
        return false;
    }

    public void setOnItemClick(final IItemClickListener<D> clickListener,
                               @NonNull @IdRes final int[] items) {
        mClickListener = clickListener;
        setClickableItems(items);
    }

    public void setOnItemTouch(@Nullable final IItemTouchListener<D> touchListener,
                               @NonNull @IdRes final int[] items) {
        mTouchListener = touchListener;
        setTouchItems(items);
    }

}

