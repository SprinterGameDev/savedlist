package com.list.sprinter.savedlist.ui.adapters;

public interface OnChangeItemsListener {

    void onMoveItem(final int pastBeforeId, final int movedId);

    void onDeleteItem(int id);

}
