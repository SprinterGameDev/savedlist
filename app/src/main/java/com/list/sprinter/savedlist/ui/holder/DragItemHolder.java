package com.list.sprinter.savedlist.ui.holder;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.utils.ItemTouchHelperViewHolder;
import com.list.sprinter.savedlist.content.models.ItemModel;

import butterknife.BindView;

public class DragItemHolder extends AbstractHolder<ItemModel> implements ItemTouchHelperViewHolder {
    @BindView(R.id.item_list_text)
    TextView mTextView;

    public DragItemHolder(@NonNull final View itemView) {
        super(itemView);
    }

    @Override
    public void bind() {
        mTextView.setText(mModel.getValue());
    }

    @Override
    public void unbind() {
        itemView.setOnTouchListener(null);
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(Color.WHITE);
    }

}
