package com.list.sprinter.savedlist.ui.adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.list.sprinter.savedlist.ui.holder.AbstractHolder;
import com.list.sprinter.savedlist.ui.holder.IItemClickListener;
import com.list.sprinter.savedlist.ui.holder.IItemTouchListener;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractRecyclerViewAdapter<D, T extends AbstractHolder> extends RecyclerView.Adapter<T> {

    @NonNull
    @IdRes
    private static final int[] CLICKABLE_LAYOUT_ITEMS = {AbstractHolder.NO_RES_ID};

    @NonNull
    @IdRes
    private static final int[] TOUCH_LAYOUT_ITEMS = {AbstractHolder.NO_RES_ID};

    protected List<D> mData;
    private IItemClickListener<D> mClickListener;
    private IItemTouchListener mTouchListener;
    private int position;
    private Set<Integer> mSelectedItems;

    public AbstractRecyclerViewAdapter() {
        this(null);
    }

    public AbstractRecyclerViewAdapter(@NonNull final Context context, @Nullable List<D> data) {
        this(data);
    }

    public AbstractRecyclerViewAdapter(@Nullable final List<D> data) {
        this.mData = data;
        mSelectedItems = new HashSet<>();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final AbstractHolder mHolder = onInitViewHolder(parent, inflater, viewType);
        mHolder.setOnItemClick(mClickListener, getClickableItems());
        mHolder.setOnItemTouch(mTouchListener, getTouchItems());
        return (T) mHolder;
    }

    public void setSelectedItem(final int position, final boolean selected) {
        if (!selected) {
            mSelectedItems.remove(position);
        } else {
            mSelectedItems.add(position);
        }
        notifyDataSetChanged();
    }

    public boolean getSelectedItem(final int position) {
        return mSelectedItems.contains(position);
    }

    @NonNull
    @IdRes
    public int[] getClickableItems() {
        return CLICKABLE_LAYOUT_ITEMS;
    }

    @NonNull
    @IdRes
    public int[] getTouchItems() {
        return TOUCH_LAYOUT_ITEMS;
    }

    /* package */
    abstract T onInitViewHolder(@NonNull final ViewGroup parent,
                                @NonNull final LayoutInflater inflater,
                                final int viewType);

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(T holder, int position) {
        this.position = holder.getAdapterPosition();
        holder.setSelected(mSelectedItems.contains(this.position));
        holder.setData(mData.get(this.position));
    }

    public int getPosition() {
        return position;
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    @Nullable
    public List<D> getData() {
        return mData;
    }

    public void setData(@Nullable final List<D> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    public void setClickListener(@Nullable final IItemClickListener<D> clickListener) {
        this.mClickListener = clickListener;
    }

    public void setTouchListener(@Nullable final IItemTouchListener<D> touchListener) {
        this.mTouchListener = touchListener;
    }

    @Override
    public void onViewRecycled(@NonNull final T holder) {
        holder.unbind();
        super.onViewRecycled(holder);
    }
}
