package com.list.sprinter.savedlist.ui.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.utils.BindLayout;

import butterknife.BindView;
import butterknife.OnClick;

@BindLayout(R.layout.dialog_fragment_create_item)
public class CreateItemDialogFragment extends AbstractDialogFragment {

    private static final String FRAGMENT_TAG = "DlgCreateItem";
    private static final String BUNDLE_ARG_REQUEST_CODE = "BUNDLE_ARG_REQUEST_CODE";
    private static final String BUNDLE_KEY_TEXT = "BUNDLE_KEY_TEXT";

    @BindView(R.id.dlg_create_item_edit)
    EditText mEditTextView;

    private int mRequestCode;

    @NonNull
    public static String getBundleText(@NonNull final Bundle bundle) {
        return bundle.getString(BUNDLE_KEY_TEXT, "");
    }

    public static <T extends Fragment & OnDialogButtonClickListener> CreateItemDialogFragment showDialog(
            @NonNull final FragmentManager manager, final int requestCode, @Nullable final T target) {

        hideDialog(manager);
        final Bundle args = new Bundle();
        args.putInt(BUNDLE_ARG_REQUEST_CODE, requestCode);

        final CreateItemDialogFragment fragment = new CreateItemDialogFragment();
        fragment.setArguments(args);

        if (target != null) {
            fragment.setTargetFragment(target, requestCode);
        }

        try {
            fragment.show(manager, FRAGMENT_TAG);
        } catch (Exception e) {
            Log.e(FRAGMENT_TAG, e.getMessage());
        }

        return fragment;
    }

    public static void hideDialog(@NonNull final FragmentManager manager) {
        final AlertDialogFragment fragment = (AlertDialogFragment) manager.findFragmentByTag(FRAGMENT_TAG);
        if (fragment != null) {
            fragment.dismissAllowingStateLoss();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle args = getArguments();
        if (args == null) {
            return;
        }
        mRequestCode = args.getInt(BUNDLE_ARG_REQUEST_CODE, NO_REQUEST_CODE);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.dlg_create_item_btn_ok)
    void onDialogButtonClick(View view) {
        final Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_TEXT, mEditTextView.getText().toString());
        onDialogButtonClick(mRequestCode, R.id.dlg_create_item_btn_ok, bundle);

        this.dismissAllowingStateLoss();
    }

}
