package com.list.sprinter.savedlist.content.contracts;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.list.sprinter.savedlist.content.DatabaseHelper;
import com.list.sprinter.savedlist.content.models.ItemModel;

import java.util.LinkedList;

public class DragItemsList extends LinkedList<ItemModel> {
    private static final int NO_ID = -1;

    public DragItemsList addAll(@NonNull final Cursor cursor) {
        final SparseArray<ItemModel> temp = new SparseArray<>();
        int nextId = NO_ID;

        if (cursor.moveToFirst()) {
            do {
                final ItemModel item = new ItemModel(cursor);

                if (item.getType() == DatabaseHelper.TYPE_FIRST) {
                    nextId = item.getId() != item.getNext() ? item.getNext() : NO_ID;
                    this.add(item);
                } else {
                    temp.put(item.getId(), item);
                }

            } while (cursor.moveToNext());

            if (nextId == NO_ID) {
                return this;
            }

            if (temp.size() <= 0) {
                temp.clear();
                return this;
            }

            do {
                final ItemModel tmpItem = temp.get(nextId);
                temp.remove(nextId);
                nextId = tmpItem.getNext();
                this.add(tmpItem);
            } while (temp.size() > 0);

            temp.clear();
        }

        return this;
    }

}
