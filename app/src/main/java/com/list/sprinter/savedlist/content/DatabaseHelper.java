package com.list.sprinter.savedlist.content;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "demoDBList.db";
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE = "mainList";
    public static final String ID_NAME_COLUMN = "_id";
    public static final String VALUE_NAME_COLUMN = "value";
    public static final String TYPE_NAME_COLUMN = "type";
    public static final String NEXT_NAME_COLUMN = "next";

    public static final int TYPE_FIRST = 0;
    public static final int TYPE_BEFORE = 1;
    public static final int TYPE_LAST = 2;

    private static final String DATABASE_CREATE_SCRIPT = "create table " +
            DATABASE_TABLE + " (" +
            ID_NAME_COLUMN + " integer primary key autoincrement, " +
            VALUE_NAME_COLUMN + " string , " +
            TYPE_NAME_COLUMN + " integer , " +
            NEXT_NAME_COLUMN + " integer);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                          int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                          int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
        onCreate(db);
    }

}
