package com.list.sprinter.savedlist.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.content.contracts.DragItemsList;
import com.list.sprinter.savedlist.content.loaders.DragItemsLoader;
import com.list.sprinter.savedlist.services.ServiceSaveList;
import com.list.sprinter.savedlist.ui.adapters.LinkedRecyclerAdapter;
import com.list.sprinter.savedlist.ui.adapters.OnChangeItemsListener;
import com.list.sprinter.savedlist.ui.fragment.dialog.CreateItemDialogFragment;
import com.list.sprinter.savedlist.ui.fragment.dialog.OnDialogButtonClickListener;
import com.list.sprinter.savedlist.utils.BindLayout;
import com.list.sprinter.savedlist.utils.SimpleDividerItemDecoration;
import com.list.sprinter.savedlist.utils.SimpleItemTouchHelperCallback;

import butterknife.BindView;
import butterknife.OnClick;

@BindLayout(R.layout.fragment_list)
public class LinkedListFragment extends BaseFragment implements LoaderCallbacks<DragItemsList>,
        OnChangeItemsListener, OnDialogButtonClickListener {

    public static final String FRAGMENT_TAG = "LinkedListFragment";
    private static final int URL_LOADER = 1233;
    private static final int DIALOG_CREATE_ITEM_REQUEST_CODE = 943;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private LinkedRecyclerAdapter mAdapter;
    private ItemTouchHelper mItemTouchHelper;
    private Loader<DragItemsList> mLoader;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new LinkedRecyclerAdapter(this);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(
                ContextCompat.getDrawable(getContext(), R.drawable.list_divider)));

        final ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);

        mLoader = getLoaderManager().initLoader(URL_LOADER, null, this);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.list_btn_create_item)
    protected void onCreateItemClick() {
        CreateItemDialogFragment.showDialog(getFragmentManager(), DIALOG_CREATE_ITEM_REQUEST_CODE,
                this);
    }

    @Override
    public Loader<DragItemsList> onCreateLoader(int id, Bundle args) {
        return new DragItemsLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<DragItemsList> loader, DragItemsList list) {
        if (loader.getId() == URL_LOADER && mAdapter != null) {
            mAdapter.setData(list);
        }
    }

    @Override
    public void onLoaderReset(Loader<DragItemsList> loader) {
    }

    @Override
    public void onDeleteItem(final int id) {
        ServiceSaveList.deleteItem(getActivity().getApplicationContext(), id);
    }

    @Override
    public void onMoveItem(final int pastBeforeId, final int movedId) {
        ServiceSaveList.moveItem(getActivity().getApplicationContext(), pastBeforeId, movedId);
    }

    @Override
    public void onDialogButtonClick(int requestCode, int buttonId, @Nullable Bundle baggage) {
        if (requestCode == DIALOG_CREATE_ITEM_REQUEST_CODE && baggage != null) {
            ServiceSaveList.addItem(getActivity().getApplicationContext(),
                    CreateItemDialogFragment.getBundleText(baggage));
        }
    }

    @Override
    public void onDestroyView() {
        if (mLoader != null) {
            mLoader.cancelLoad();
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
        }

        if (mAdapter != null) {
            mAdapter.setData(null);
        }

        mLoader = null;
        mItemTouchHelper = null;
        mAdapter = null;
        mRecyclerView = null;

        super.onDestroyView();
    }

}
