package com.list.sprinter.savedlist.ui.adapters;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.content.models.ItemModel;
import com.list.sprinter.savedlist.ui.holder.DragItemHolder;
import com.list.sprinter.savedlist.utils.ItemTouchHelperAdapter;

public class LinkedRecyclerAdapter extends AbstractRecyclerViewAdapter<ItemModel, DragItemHolder>
        implements ItemTouchHelperAdapter {


    @NonNull
    @IdRes
    private static final int[] TOUCH_LAYOUT_ITEMS = {R.id.item_list_drag_handle};

    private OnChangeItemsListener mOnChangeItemsListener = null;
    private boolean mInMovedItem = false;
    private int mSelectItemId = -1;

    public LinkedRecyclerAdapter(@NonNull final OnChangeItemsListener onChangeItemsListener) {
        mOnChangeItemsListener = onChangeItemsListener;
    }

    @Override
    protected DragItemHolder onInitViewHolder(@NonNull final ViewGroup parent,
                                              @NonNull final LayoutInflater inflater,
                                              final int viewType) {
        return new DragItemHolder(inflater.inflate(R.layout.item_list_draggable, parent, false));
    }

    @Override
    public long getItemId(int position) {
        return mData.get(position).getId();
    }

    @NonNull
    @IdRes
    public int[] getTouchItems() {
        return TOUCH_LAYOUT_ITEMS;
    }

    @Override
    public void onItemDismiss(int position) {
        if (mData.size() > position) {
            mOnChangeItemsListener.onDeleteItem(mData.get(position).getId());
        }
        mData.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition, boolean touchEnd) {
        int beforeId = -1;

        if (!touchEnd) {
            if (!mInMovedItem) {
                mSelectItemId = mData.get(fromPosition).getId();
            }
            mInMovedItem = true;
            final ItemModel item = mData.remove(fromPosition);
            mData.add(toPosition, item);
            notifyItemMoved(fromPosition, toPosition);
            return;
        }

        if (mData.size() > toPosition + 1) {
            beforeId = mData.get(toPosition + 1).getId();
        }

        mInMovedItem = false;

        if (beforeId == -1 && mData.size() >= 2 && toPosition + 1 < mData.size()) {
            beforeId = mData.get(1).getId();
        }

        mOnChangeItemsListener.onMoveItem(beforeId, mSelectItemId);
    }

}
