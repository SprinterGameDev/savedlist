package com.list.sprinter.savedlist.content.models;

import android.database.Cursor;
import android.support.annotation.NonNull;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import lombok.Data;

@Data
@Parcel
public class ItemModel {

    final int id;
    final String value;
    final int type;
    final int next;

    @ParcelConstructor
    public ItemModel(final int id, @NonNull final String value, final int type, final int next) {
        this.id = id;
        this.value = value;
        this.type = type;
        this.next = next;
    }

    public ItemModel(@NonNull final Cursor cursor) {
        id = cursor.getInt(0);
        value = cursor.getString(1);
        type = cursor.getInt(2);
        next = cursor.getInt(3);
    }

}
