package com.list.sprinter.savedlist.ui.holder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;

public interface IItemTouchListener<D> {

    void onItemTouch(@NonNull final AbstractHolder<D> holder, @NonNull final D data,
                     @NonNull final MotionEvent event, @NonNull final View view,
                     @Nullable final String tag);

}
