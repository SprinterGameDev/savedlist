package com.list.sprinter.savedlist.ui.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.utils.BindLayout;

import butterknife.BindView;
import butterknife.OnClick;

@BindLayout(R.layout.dialog_fragment_alert)
public class AlertDialogFragment extends AbstractDialogFragment {

    public static final int BUTTON_POSITIVE_ID = 0;
    public static final int BUTTON_NEGATIVE_ID = 1;

    private static final String BUNDLE_ARG_TITLE = "BUNDLE_ARG_TITLE";
    private static final String BUNDLE_ARG_MESSAGE = "BUNDLE_ARG_MESSAGE";
    private static final String BUNDLE_ARG_BUTTON_POSITIVE = "BUNDLE_ARG_BUTTON_POSITIVE";
    private static final String BUNDLE_ARG_BUTTON_NEGATIVE = "BUNDLE_ARG_BUTTON_NEGATIVE";
    private static final String FRAGMENT_TAG = "AlertDialogFragment";
    private static final String BUNDLE_ARG_REQUEST_CODE = "BUNDLE_ARG_REQUEST_CODE";
    private static final String BUNDLE_ARG_SINGLE_BUTTON = "BUNDLE_ARG_SINGLE_BUTTON";
    private static final String BUNDLE_DIALOG_BAGGAGE = "BUNDLE_DIALOG_BAGGAGE";

    @BindView(R.id.dialog_title)
    protected TextView mTitle;
    @BindView(R.id.dialog_text_message)
    protected TextView mMessage;
    @BindView(R.id.dialog_button_negative)
    protected TextView mBtnNegative;
    @BindView(R.id.dialog_button_positive)
    protected TextView mBtnPositive;
    private int mRequestCode;
    @Nullable
    private Bundle mBaggage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestCode = NO_REQUEST_CODE;
    }

    public static <T extends Fragment & OnDialogButtonClickListener> AlertDialogFragment showDialog(
            @NonNull final FragmentManager manager,
            @Nullable final String title,
            @Nullable final String message) {
        return showDialog(manager, -1, null, title, message, true, null, null, null);
    }

    public static <T extends Fragment & OnDialogButtonClickListener> AlertDialogFragment showDialog(
            @NonNull final FragmentManager manager,
            final int requestCode,
            @Nullable final T target,
            @Nullable final String title,
            @Nullable final String message,
            final boolean singleButton,
            @Nullable final String btnPositive,
            @Nullable final String btnNegative,
            @Nullable final Bundle baggage) {

        final Bundle args = new Bundle();
        args.putString(BUNDLE_ARG_TITLE, title);
        args.putString(BUNDLE_ARG_MESSAGE, message);
        args.putBoolean(BUNDLE_ARG_SINGLE_BUTTON, singleButton);
        args.putString(BUNDLE_ARG_BUTTON_POSITIVE, btnPositive);
        args.putString(BUNDLE_ARG_BUTTON_NEGATIVE, btnNegative);
        args.putInt(BUNDLE_ARG_REQUEST_CODE, requestCode);
        if (baggage != null) {
            args.putParcelable(BUNDLE_DIALOG_BAGGAGE, baggage);
        }
        hideDialog(manager);

        final AlertDialogFragment fragment = new AlertDialogFragment();
        fragment.setArguments(args);

        if (target != null) {
            fragment.setTargetFragment(target, requestCode);
        }

        try {
            fragment.show(manager, FRAGMENT_TAG);
        } catch (Exception e) {
            Log.e(FRAGMENT_TAG, e.getMessage());
        }

        return fragment;
    }

    public static void hideDialog(@NonNull final FragmentManager manager) {
        final AlertDialogFragment fragment = (AlertDialogFragment) manager.findFragmentByTag(FRAGMENT_TAG);
        if (fragment != null) {
            fragment.dismissAllowingStateLoss();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Bundle args = getArguments();
        if (args == null) {
            return;
        }
        mBaggage = args.getParcelable(BUNDLE_DIALOG_BAGGAGE);
        mTitle.setText(args.getString(BUNDLE_ARG_TITLE, ""));
        mMessage.setHint(args.getString(BUNDLE_ARG_MESSAGE, ""));
        mBtnPositive.setText(args.getString(BUNDLE_ARG_BUTTON_POSITIVE,
                getString(R.string.dialog_button_ok)));
        mBtnNegative.setText(args.getString(BUNDLE_ARG_BUTTON_NEGATIVE,
                getString(R.string.dialog_button_cancel)));
        mRequestCode = args.getInt(BUNDLE_ARG_REQUEST_CODE, NO_REQUEST_CODE);

        if (args.getBoolean(BUNDLE_ARG_SINGLE_BUTTON)) {
            mBtnNegative.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("unused")
    @OnClick({R.id.dialog_button_positive, R.id.dialog_button_negative})
    void onDialogButtonClick(View view) {
        switch (view.getId()) {
            case R.id.dialog_button_positive:
                onDialogButtonClick(mRequestCode, BUTTON_POSITIVE_ID, mBaggage);
                break;

            case R.id.dialog_button_negative:
                onDialogButtonClick(mRequestCode, BUTTON_NEGATIVE_ID, mBaggage);
                break;

            default:
                Log.i(FRAGMENT_TAG, "click on view with undefined id");
        }

        this.dismissAllowingStateLoss();
    }

}
