package com.list.sprinter.savedlist.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.list.sprinter.savedlist.ui.activity.BaseActivity;
import com.list.sprinter.savedlist.utils.BindLayout;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;

public abstract class BaseFragment extends Fragment {

    private Unbinder mUnbinder;
    protected boolean mIsViewBinded;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final BindLayout annotation = getInjectLayoutAnnotation();
        if (annotation != null) {
            return inflateAndInject(annotation.value(), inflater, container);
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    private BindLayout getInjectLayoutAnnotation() {
        BindLayout annotation;
        Class typeToLookUp = getClass();
        while (true) {
            annotation = (BindLayout) typeToLookUp.getAnnotation(BindLayout.class);
            if (annotation != null) {
                break;
            }
            typeToLookUp = typeToLookUp.getSuperclass();
            if (typeToLookUp == null) {
                break;
            }
        }
        return annotation;
    }

    public void switchFragment(Fragment fragment, boolean clearBackStack, String tag) {
        final FragmentActivity activity = getActivity();
        if (activity instanceof BaseActivity) {
            ((BaseActivity) activity).switchFragment(fragment, clearBackStack, tag);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mIsViewBinded = true;
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    protected View inflateAndInject(int layoutId, LayoutInflater inflater, ViewGroup container) {
        final View view = inflater.inflate(layoutId, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    protected void hideSoftKeyboard() {
        if (getActivity() != null && getActivity().getCurrentFocus() != null) {
            final InputMethodManager inputManager = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    protected void showKeyboard(@Nullable final View view) {
        showKeyboard(view, false);
    }

    protected void showKeyboard(@Nullable final View v, final boolean forceShow) {
        if (v != null) {
            final InputMethodManager inputManager = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                final int flags = (forceShow ? InputMethodManager.SHOW_FORCED : InputMethodManager.SHOW_IMPLICIT);
                inputManager.showSoftInput(v, flags);
            }
        }
    }

    @Override
    public void onDestroyView() {
        hideSoftKeyboard();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        mIsViewBinded = false;
        super.onDestroyView();
    }

}
