package com.list.sprinter.savedlist.ui.activity;

import android.os.Bundle;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.ui.fragment.LinkedListFragment;
import com.list.sprinter.savedlist.utils.BindLayout;

@BindLayout(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            switchFragment(new LinkedListFragment(), true, LinkedListFragment.FRAGMENT_TAG);
        }
    }

}
