package com.list.sprinter.savedlist.content.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.list.sprinter.savedlist.manager.DataBaseManager;
import com.list.sprinter.savedlist.content.DatabaseHelper;
import com.list.sprinter.savedlist.content.models.ItemModel;

import java.util.HashMap;
import java.util.Map;

public class LinkedListProvider extends ContentProvider {

    private static final String TAG = "LinkedListProvider";

    public static final String VALUE_UPDATE_MOVED_ID = "movedId";
    public static final String VALUE_UPDATE_PAST_BEFORE_ID = "beforeId";

    public static final String AUTHORITY = "com.list.sprinter.savedlist.content.providers.LinkedListProvider";
    public static final String SCHEME = "content://";
    public static final String PATH_ITEMS = "/mainList";
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google." + DatabaseHelper.VALUE_NAME_COLUMN;

    public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_ITEMS);
    public static final String[] DEFAULT_PROJECTION = new String[]{
            DatabaseHelper.ID_NAME_COLUMN,
            DatabaseHelper.VALUE_NAME_COLUMN,
            DatabaseHelper.TYPE_NAME_COLUMN,
            DatabaseHelper.NEXT_NAME_COLUMN
    };

    public static final String TABLE_NAME = "mainList";
    public static final String PATH_ITEMS_ID = "/mainList/";
    public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_ITEMS_ID);
    public static final int ITEMS = 1;
    public static final int NO_ID = -1;

    @NonNull
    private final UriMatcher mUriMatcher;
    @NonNull
    private final Map<String, String> mProjections;

    public LinkedListProvider() {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(AUTHORITY, TABLE_NAME, ITEMS);

        mProjections = new HashMap<>();
        for (final String projection : DEFAULT_PROJECTION) {
            mProjections.put(projection, projection);
        }
    }

    @Override
    public boolean onCreate() {
        DataBaseManager.initializeInstance(new DatabaseHelper(getContext()));
        return true;
    }

    @NonNull
    public String getType(@NonNull final Uri uri) {
        switch (mUriMatcher.match(uri)) {
            case ITEMS:
                return CONTENT_TYPE;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @NonNull
    public Cursor query(@NonNull final Uri uri, @Nullable final String[] projection,
                        @Nullable final String selection, @Nullable final String[] selectionArgs,
                        @Nullable final String sortOrder) {
        final SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();

        switch (mUriMatcher.match(uri)) {
            case ITEMS:
                qb.setTables(TABLE_NAME);
                qb.setProjectionMap(mProjections);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        final Context context = getContext();
        final Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, null);
        if (context != null) {
            cursor.setNotificationUri(context.getContentResolver(), uri);
        }
        return cursor;
    }

    public int update(@NonNull final Uri uri, @Nullable final ContentValues values,
                      @Nullable final String where, @Nullable final String[] whereArgs) {
        if (mUriMatcher.match(uri) != ITEMS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        try {
            final int pastBeforeId = values.getAsInteger(VALUE_UPDATE_PAST_BEFORE_ID);
            final int movedId = values.getAsInteger(VALUE_UPDATE_MOVED_ID);
            return moveItem(pastBeforeId, movedId);
        } catch (Exception error) {
            Log.i(TAG, error.getMessage());
            error.printStackTrace();
        }
        return NO_ID;
    }

    @Nullable
    public Uri insert(@NonNull final Uri uri, @Nullable final ContentValues initialValues) {
        if (mUriMatcher.match(uri) != ITEMS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        return initialValues == null ? null : createItem(initialValues, NO_ID);
    }

    public int delete(@NonNull final Uri uri, @Nullable final String where,
                      @Nullable final String[] whereArgs) {
        if (mUriMatcher.match(uri) != ITEMS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        final Context context = getContext();
        int count = deleteItem(Long.parseLong(where));
        if (context != null) {
            context.getContentResolver().notifyChange(uri, null);
        }

        return count;
    }

    @Nullable
    private ItemModel getLastItemId() {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final String query = DatabaseHelper.TYPE_NAME_COLUMN + "=" +
                String.valueOf(DatabaseHelper.TYPE_LAST);
        final Cursor cursor = db.query(
                TABLE_NAME,
                DEFAULT_PROJECTION,
                query,
                null, null, null, null, "1"
        );
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            return new ItemModel(cursor);
        }

        return null;
    }

    @Nullable
    private ItemModel getFirstItemId() {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final String query = DatabaseHelper.TYPE_NAME_COLUMN + "=" +
                String.valueOf(DatabaseHelper.TYPE_FIRST);
        final Cursor cursor = db.query(
                TABLE_NAME,
                DEFAULT_PROJECTION,
                query,
                null, null, null, null, "1"
        );
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            return new ItemModel(cursor);
        }

        return null;
    }

    @Nullable
    private ItemModel getBeforeItem(final long id) {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final String query = DatabaseHelper.NEXT_NAME_COLUMN + "=" + String.valueOf(id) +
                " AND " + DatabaseHelper.ID_NAME_COLUMN + "!=" + id;

        final Cursor cursor = db.query(
                TABLE_NAME,
                DEFAULT_PROJECTION,
                query,
                null, null, null, null, "1"
        );

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            return new ItemModel(cursor);
        }

        return null;
    }

    @Nullable
    private ItemModel getAfterItem(final long id) {
        final ItemModel model = getItem(id);
        if (model == null) {
            return null;
        }

        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final String query = DatabaseHelper.ID_NAME_COLUMN + "=" + String.valueOf(model.getNext());

        final Cursor cursor = db.query(
                TABLE_NAME,
                DEFAULT_PROJECTION,
                query,
                null, null, null, null, "1"
        );

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            return new ItemModel(cursor);
        }

        return null;
    }

    @Nullable
    private ItemModel getItem(final long id) {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final String query = DatabaseHelper.ID_NAME_COLUMN + "=" + String.valueOf(id);

        final Cursor cursor = db.query(
                TABLE_NAME,
                DEFAULT_PROJECTION,
                query,
                null, null, null, null, "1"
        );

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            return new ItemModel(cursor);
        }

        return null;
    }

    public Uri createItem(@NonNull final ContentValues values, final int pastBeforeId) {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        values.put(DatabaseHelper.NEXT_NAME_COLUMN, 0);

        final ItemModel lastItemId = getLastItemId();
        final ItemModel firstItemId = getFirstItemId();
        ItemModel currentItem = null;
        ItemModel beforeItem = null;

        if (pastBeforeId > NO_ID) {
            currentItem = getItem(pastBeforeId);
            if (currentItem == null && lastItemId != null) {
                currentItem = lastItemId;
            }
            if (currentItem != null) {
                beforeItem = getBeforeItem(currentItem.getId());
            }

        } else {
            if (firstItemId == null) {
                values.put(DatabaseHelper.TYPE_NAME_COLUMN, DatabaseHelper.TYPE_FIRST);

            } else {
                values.put(DatabaseHelper.TYPE_NAME_COLUMN, DatabaseHelper.TYPE_LAST);
                currentItem = firstItemId;

            }

            if (lastItemId != null) {
                currentItem = lastItemId;
            }
        }

        final long rowId = db.insert(DatabaseHelper.DATABASE_TABLE,
                DatabaseHelper.VALUE_NAME_COLUMN, values);

        if (pastBeforeId == -1 && currentItem != null) {
            writeNextId(currentItem.getId(), rowId);
            if (currentItem.getType() == DatabaseHelper.TYPE_LAST) {
                writeType(currentItem.getId(), DatabaseHelper.TYPE_BEFORE);
            }

        } else if (pastBeforeId > -1 && currentItem != null) {
            switch (currentItem.getType()) {
                case DatabaseHelper.TYPE_FIRST:
                    writeType(rowId, DatabaseHelper.TYPE_FIRST);
                    writeNextId(rowId, currentItem.getId());
                    if (lastItemId == null) {
                        writeType(currentItem.getId(), DatabaseHelper.TYPE_LAST);
                        writeNextId(currentItem.getId(), 0);

                    } else {
                        writeType(currentItem.getId(), DatabaseHelper.TYPE_BEFORE);
                    }
                    break;

                case DatabaseHelper.TYPE_LAST:
                case DatabaseHelper.TYPE_BEFORE:
                    writeType(rowId, DatabaseHelper.TYPE_BEFORE);
                    writeNextId(rowId, currentItem.getId());
                    if (beforeItem != null) {
                        writeNextId(beforeItem.getId(), rowId);
                    }
                    break;
            }
        }

        final Uri rowUri = ContentUris.withAppendedId(CONTENT_ID_URI_BASE, rowId);
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(rowUri, null);
        }

        return rowUri;
    }

    private int deleteItem(final long id) {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final ItemModel beforeItem = getBeforeItem(id);
        final ItemModel afterItem = getAfterItem(id);
        final String query = DatabaseHelper.ID_NAME_COLUMN + "=" + String.valueOf(id);

        int count = db.delete(TABLE_NAME, query, null);

        if (beforeItem == null && afterItem == null) {
            return 1;
        } else if (beforeItem == null) {
            writeType(afterItem.getId(), DatabaseHelper.TYPE_FIRST);

        } else if (afterItem == null && beforeItem.getType() != DatabaseHelper.TYPE_FIRST) {
            writeType(beforeItem.getId(), DatabaseHelper.TYPE_LAST);
            writeNextId(beforeItem.getId(), 0);

        } else if (afterItem != null && beforeItem != null) {
            writeNextId(beforeItem.getId(), afterItem.getId());

        } else if (afterItem == null) {
            writeNextId(beforeItem.getId(), 0);
        }

        return count;
    }

    private int moveItem(final int beforeId, final int movedId) {
        if (beforeId == movedId) {
            return 0;
        }

        final ContentValues values = new ContentValues();
        final ItemModel movedItem = getItem(movedId);

        if (movedItem != null) {
            values.put(DatabaseHelper.VALUE_NAME_COLUMN, movedItem.getValue());
        }

        deleteItem(movedId);
        createItem(values, beforeId);

        return 1;
    }

    private void writeNextId(final long id, final long nextId) {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final ContentValues values = new ContentValues();
        values.put(DatabaseHelper.NEXT_NAME_COLUMN, nextId);
        db.update(TABLE_NAME, values, DatabaseHelper.ID_NAME_COLUMN + "=" + String.valueOf(id),
                null);
    }

    private void writeType(final long id, final long type) {
        final SQLiteDatabase db = DataBaseManager.getInstance().openDatabase();
        final ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TYPE_NAME_COLUMN, type);
        db.update(TABLE_NAME, values, DatabaseHelper.ID_NAME_COLUMN + "=" + String.valueOf(id), null);
    }

}
