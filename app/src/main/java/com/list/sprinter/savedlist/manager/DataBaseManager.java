package com.list.sprinter.savedlist.manager;

import android.database.sqlite.SQLiteDatabase;

import com.list.sprinter.savedlist.content.DatabaseHelper;

public class DataBaseManager {
    private static volatile DataBaseManager sInstance;
    private static DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    private int mOpenCounter;

    public static synchronized void initializeInstance(DatabaseHelper helper) {
        getInstance();
        mDatabaseHelper = helper;
    }

    public static DataBaseManager getInstance() {
        DataBaseManager localInstance = sInstance;
        if (localInstance == null) {
            synchronized (DataBaseManager.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new DataBaseManager();
                }
            }
        }
        return localInstance;
    }

    public synchronized SQLiteDatabase openDatabase() {
        mOpenCounter++;
        if (mOpenCounter == 1) {
            mDatabase = mDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }

    public synchronized void closeDatabase() {
        mOpenCounter--;
        if (mOpenCounter == 0) {
            mDatabase.close();
        }
    }

}
