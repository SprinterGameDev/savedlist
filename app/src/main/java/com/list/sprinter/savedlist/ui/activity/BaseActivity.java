package com.list.sprinter.savedlist.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.list.sprinter.savedlist.R;
import com.list.sprinter.savedlist.ui.fragment.dialog.AlertDialogFragment;
import com.list.sprinter.savedlist.ui.fragment.dialog.OnDialogButtonClickListener;
import com.list.sprinter.savedlist.utils.BindLayout;

import butterknife.ButterKnife;
import icepick.Icepick;

public class BaseActivity extends AppCompatActivity implements OnDialogButtonClickListener {

    private static final String TAG = "BaseActivity";
    private static final int REQUEST_CODE_DIALOG_EXIT = 3425;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final BindLayout layout = getClass().getAnnotation(BindLayout.class);
        if (layout != null) {
            setContentView(layout.value());
            ButterKnife.bind(this, this);
        }
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    public void switchFragment(@NonNull final Fragment fragment, final boolean clearBackStack,
                               @NonNull final String tag) {
        final FragmentManager fragmentManager = getSupportFragmentManager();

        if (clearBackStack) {
            try {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (IllegalStateException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        try {
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(tag)
                    .add(R.id.main_container, fragment, tag)
                    .commitAllowingStateLoss();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        final InputMethodManager inputMethodManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        final View view = this.getCurrentFocus() == null ? new View(this) : this.getCurrentFocus();
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        if (backStackEntryCount > 0) {
            if (backStackEntryCount > 1) {
                onActivityBackPressed();
            } else {
                onExitFromApp();
            }
        } else {
            onActivityBackPressed();
        }
    }

    protected void onExitFromApp() {
        AlertDialogFragment.showDialog(getSupportFragmentManager(),
                REQUEST_CODE_DIALOG_EXIT, null, getString(R.string.app_name),
                getString(R.string.dialog_exit_app_message), false,
                getString(R.string.dialog_button_yes),
                getString(R.string.dialog_button_no),
                null);
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key. The default implementation simply finishes the current activity,
     * but you can override this to do whatever you want.
     */
    public void onActivityBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onDialogButtonClick(int requestCode, int buttonId, @Nullable Bundle value) {
        if (requestCode == REQUEST_CODE_DIALOG_EXIT) {
            if (buttonId == AlertDialogFragment.BUTTON_POSITIVE_ID) {
                supportFinishAfterTransition();
            }
        }
    }

}
